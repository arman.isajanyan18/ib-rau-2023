# Use the official Alpine image as the base image
FROM alpine:latest

# Set the working directory
WORKDIR /app

# Install necessary packages (CMake, g++, etc.)
RUN apk update && apk add cmake g++ make poco-dev

# Copy your C++ source files into the container
COPY . .

# Build your C++ application
RUN mkdir build && \
    cd build && \
    cmake .. && \
    make VERBOSE=1


# Set the command to run your application
CMD ["./build/armanapp"]
